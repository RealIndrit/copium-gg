var scroll_position = 0;
var page_title;

async function showPlayerCard(element) {
    $.getJSON('assets/profiles/players.json', function(data) {
        takePageSnapShot();
        $.each(data, function(index, player) {
            if (player.name == element.alt) {
                $(document.body).addClass('w3-disable-scroll');
                $("meta[property='og:title']").attr("content", page_title + " - " + player.name)
                $("meta[property='og:description']").attr("content", "Copium player " + player.name + " in team " + player.group)
                $('#playerCardImg').attr("src", player.picture)
                $('#playerCard').css({ "display": "block" })
                $('#playerCardName').text("Name: " + player.name);
                $('#playerCardAge').text("Age: " + player.age);
                $('#playerCardCountry').text("Country: " + player.country);
                $('#playerCardDescription').text(player.description);
                $('#socials-panel').empty();

                if (Object.keys(player.socials).length) {
                    $('#socials-panel').append(`<p class="w3-opacity w3-xlarge" style="padding-top: 32px;">Socials:</p>`);
                    $('#socials-panel').append(`<div id="socials-row" class="w3-auto-row"></div>`)
                    for (const x in player.socials) {
                        $('#socials-row').append(`<a href="${player.socials[x]}" target="_blank"><i class="fab fa-${x} fa-3x w3-hover-opacity" title="${x}"></i></a>`)
                    }
                    $('#socials-row').css({ "margin-inline": `${100/(Object.keys(player.socials).length + 1)}%` })
                }
                return;
            }
        });
    });
};

function takePageSnapShot() {
    // Save key values for the page
    scroll_position = $(window).scrollTop();
    page_title = $("meta[property='og:title']").attr("content");
    page_description = $("meta[property='og:description']").attr("content");
}

function resetPageSnapShot() {
    // Reset key values for the page
    $(window).scrollTop(scroll_position);
    $("meta[property='og:title']").attr("content", page_title);
    $("meta[property='og:description']").attr("content", page_description)
}

function hidePlayerCard() {
    $(document.body).removeClass('w3-disable-scroll');
    resetPageSnapShot();
    $('#playerCard').css({ "display": "none" })
};

async function preLoad() {
    // code here will run after DOM loads
    $("#playerCardHtml").load("player_card.html");
    $.getJSON('assets/profiles/players.json', function(data) {
        $.each(data, function(index, player) {
            if (!$(`#group${player.group}`).length)
                $('#playersListHtml').append(`<div id="group${player.group}" class="w3-row-padding w3-center">
                <h3 class="w3-container w3-center"><em>Copium ${player.group}</em></h3></div>`);
            $(`#group${player.group}`).append(`<div class="w3-column w3-sizing m2 s4">
            <img src="${player.picture}" style="width:100%;" onclick="showPlayerCard(this)" class="w3-cropped w3-hover-opacity w3-circle" alt="${player.name}">
            <p>${player.name}</p><br>
            </div>`)
        });
    });
};

// Used to toggle the menu on small screens when clicking on the menu button
function toggleNavbar() {
    var x = document.getElementById("navTablet");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
};